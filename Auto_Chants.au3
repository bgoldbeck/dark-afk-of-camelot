#include <ButtonConstants.au3>
#include <GuiConstantsEx.au3>
#include <Misc.au3>
#include <EditConstants.au3>
#include "AutoIT_ImageSearch/_ImageSearch.au3"
#include "AutoIT_ImageSearch/_ImageSearch_Debug.au3"

#requireadmin


Global $endoImage = @ScriptDir & "\Images\endo_pulse.bmp"
Global $combatImage = @ScriptDir & "\Images\combat_mode.bmp"
Global $imageSearchTolerance = 20
Global $chantTimer = TimerInit()
Global $chantCooldown = 8 ; Seconds


While True
   Sleep(50)
   If WinActive("Dark Age", "") Then
	  Local $dif = TimerDiff($chantTimer)
	  If $dif > ($chantCooldown * 1000) Then
		If IsInCombat() Then
			$chantTimer = TimerInit()
			PlayChants()
		 EndIf
		 If HasEndoPlaying() == False Then
		    ControlSend("Dark Age", "", "", "{F11}")
		    Sleep(500)
		 EndIf
	  EndIf

   EndIf
WEnd

Func PlayChants()
	  ControlSend("Dark Age", "", "", "{F6}")
	  ControlSend("Dark Age", "", "", "{F6}")
	  Sleep(50)
	  ControlSend("Dark Age", "", "", "{F6}")
	  Sleep(100)
	  ControlSend("Dark Age", "", "", "{F7}")
	  ControlSend("Dark Age", "", "", "{F7}")
	  Sleep(50)
	  ControlSend("Dark Age", "", "", "{F7}")
	  Sleep(100)
	  ControlSend("Dark Age", "", "", "{F8}")
	  ControlSend("Dark Age", "", "", "{F8}")
	  Sleep(50)
	  ControlSend("Dark Age", "", "", "{F8}")
	  Sleep(100)
	  ControlSend("Dark Age", "", "", "{F9}")
	  ControlSend("Dark Age", "", "", "{F9}")
	  Sleep(50)
	  ControlSend("Dark Age", "", "", "{F9}")
	  Sleep(100)
	  ControlSend("Dark Age", "", "", "{F10}")
	  Sleep(200)
EndFunc

Func IsInCombat()
   Local $success = False
   Local $imageResult = _ImageSearch($combatImage, $imageSearchTolerance)

   If $imageResult[0] == 1 Then
	  $success = True
   EndIf

   return $success
EndFunc


Func HasEndoPlaying()
   Local $success = False
   Local $imageResult = _ImageSearch($endoImage, $imageSearchTolerance)

   If $imageResult[0] == 1 Then
	  $success = True
   EndIf

   return $success
EndFunc