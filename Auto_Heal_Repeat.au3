#include <ButtonConstants.au3>
#include <GuiConstantsEx.au3>
#include <Misc.au3>
#include <EditConstants.au3>
#include "AutoIT_ImageSearch/_ImageSearch.au3"
#include "AutoIT_ImageSearch/_ImageSearch_Debug.au3"

#requireadmin

Global $programName = "Auto Heal Repeat"

; Test to make sure there is not already an instance running.
If _Singleton($programName, 1) = 0 Then
    Exit
 EndIf

Global $hpImage = @ScriptDir & "\Images\hp-15.bmp"
Global $imageSearchTolerance = 65


While True
   Sleep(10)

   Local $foundMissingHealth = _ImageSearch($hpImage, $imageSearchTolerance)

   If $foundMissingHealth[0] == 1 Then
	  MouseClick("Left", $foundMissingHealth[1], $foundMissingHealth[2], 1, 10)
	  Sleep(100)
	  ControlSend("Dark Age", "", "", "3") ; Heal
	  Sleep(2000)
	  ControlSend("Dark Age", "", "", "{ESC}")
	  Sleep(100)
   EndIf

   $msg = Guigetmsg()
   Select
	   Case $msg = $GUI_EVENT_CLOSE
		 Exit
   EndSelect


WEnd