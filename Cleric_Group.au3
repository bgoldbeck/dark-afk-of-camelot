
; # SECONDS between class timer in combat. (EG: Quickcast spell every 30 seconds)
Local $timerRecast = 30

; # MINUTES between recast class buffs.
Local $buffRecast = 20

; # SECONDS between your ability to pull a mob.
local $pullRecast = 3


; Spells on bar
; Slot 1:
; Slot 2:
; Slot 3: Your single target heal of choice.
; Slot 4:
; Slot 5:
; Slot 6:
; Slot 7:
; Slot 8:
; Slot 9:
; Slot 0: Group Heal Proc Buff


; This is called every '$timerRecast' seconds while in combat.
Func ClassTimer()
   ; Start Editing here
EndFunc


; This is called every time the script gains a new target.
Func ClassPull()
   ; Start Editing here
EndFunc


; This is called on repeat while the script has a target.
Func ClassAttack()
   ; Start Editing here

EndFunc


; This is called every '$buffRecast' minutes and only while the script has no target.
Func ClassBuff()
   ; Start Editing here
   ControlSend("Dark Age", "", "", "0") ; Group Heal Proc
   Sleep(2800)
EndFunc

; This is called every rotation, regardless of combat or not.
Func ClassUpdate()
   ; Start Editing here
   ShellExecute("Auto_Heal.au3")
   Sleep(2350)
EndFunc

; ----------------------------------------------------------------------------------------------;
; DO NOT EDIT ANYTHING BELOW THIS LINE
; ----------------------------------------------------------------------------------------------;
Local $buffTimer = 999999999
Local $classTimer = 999999999
Local $pullTimer = 99999

Func ClassPullImp($statusButton)
   Local $pullTimerDif = Timerdiff($pullTimer)
   While $pullTimerDif < ($pullRecast * 1000)
	  GUICtrlSetData($statusButton, "Status:  Waiting to pull.")
   WEnd
	  $pullTimer = Timerinit()
	  GUICtrlSetData($statusButton, "Status:  Pulling")
   ClassPull()
EndFunc

Func ClassBuffImp($statusButton)
   Local $buffTimerDif = Timerdiff($buffTimer)
   If $buffTimerDif > ($buffRecast * 60000) Then
	  GUICtrlSetData($statusButton, "Status:  Buffing.")
	  ClassBuff()
	  $buffTimer = Timerinit()
   EndIf
EndFunc

Func ClassTimerImp($statusButton)
   Local $classTimerDif = Timerdiff($classTimer)
   If $classTimerDif > ($timerRecast * 1000) Then
	  GUICtrlSetData($statusButton, "Status:  Hitting Class Timers")
	  ClassTimer()
	  $classTimer = Timerinit()
   EndIf
EndFunc