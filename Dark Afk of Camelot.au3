; INCLUE THE SCRIPT OF THE CLASS YOU WANT TO AFK WITH.

#include "Necro_Deathsight_Bridge.au3"

; ----------------------------------------------------------------------------------------------;
; DO NOT EDIT ANYTHING BELOW THIS LINE
; ----------------------------------------------------------------------------------------------;
#include <ButtonConstants.au3>
#include <GuiConstantsEx.au3>
#include <Misc.au3>
#include <EditConstants.au3>
#include "AutoIT_ImageSearch/_ImageSearch.au3"
#include "AutoIT_ImageSearch/_ImageSearch_Debug.au3"

#requireadmin

#NoTrayIcon


HotKeySet("{PAUSE}", "TogglePause")
If _Singleton("AFK",1) = 0 Then
    Exit
EndIf

Local $timeout = 45

local $windowWidth = 275
Local $windowHeight = 240

$gui = GUICreate("AFKER", $windowWidth, $windowHeight, 2,-1,-1)


; GUI Elements
$startButton = GUICtrlCreateButton("Start", 0, 0, 275, 45)
GUICtrlSetState($startButton, $GUI_DEFBUTTON)
GUICtrlSetFont(-1, 12, 400, 0, "Tahoma")

$stopButton = GUICtrlCreateButton("Stop", 0, 45, 275, 45)
GUICtrlSetState($stopButton, $GUI_DEFBUTTON)
GUICtrlSetFont(-1, 12, 400, 0, "Tahoma")

$pauseInfoLabel = GUICtrlCreateLabel("Pause Key will Pause  ", 65, $windowHeight - 130, $windowWidth, 45)
GUICtrlSetFont(-1, 12, 400, 0, "Tahoma")

$statusButton = GUICtrlCreateButton("Status:  ", 0, $windowHeight - 100, $windowWidth, 45)
GUICtrlSetState($statusButton, $GUI_DEFBUTTON)
GUICtrlSetFont(-1, 12, 400, 0, "Tahoma")

; Create a combobox control.
GUICtrlCreateLabel("Search Key  ", 5, $windowHeight - 40, $windowWidth, 20)
$tabOrAssistComboBox = GUICtrlCreateCombo("{TAB}", 5, $windowHeight - 25, $windowWidth - 10, 20)

; Add additional items to the combobox.
GUICtrlSetData($tabOrAssistComboBox, "{TAB}|0|1|2|3|4|5|6|7|8|9|-|=")


; Make GUI Show up
GUISetState(@SW_SHOW)
Global $isStarted = False

Local $paused = false

Local $timeoutTimer = TimerInit()

Local $onTarget = False

Global $hitImage = @ScriptDir & "\Images\hit_target.bmp"
Global $imageSearchTolerance = 65


While True
	If Not WinActive("Dark Age", "") and $isStarted = False Then
		GUICtrlSetData($statusButton, "Status:  Stopped")
	EndIf
	If Not WinActive("Dark Age", "") and $isStarted = True Then
		GUICtrlSetData($statusButton, "Status:  Waiting for DAoC Window")
	EndIf
	sleep(30)


   $msg = Guigetmsg()

   Select
	   Case $msg = $GUI_EVENT_CLOSE
		 exit
	  Case $msg = $stopButton
		 $isStarted = False
	  Case $msg = $startButton
		 $isStarted = True
	  EndSelect

	If WinActive("Dark Age", "") and $isStarted = true Then
		Sleep(10)
	    ClassUpdate()

	    Local $hasTarget = _ImageSearch($hitImage, $imageSearchTolerance)
	    GUICtrlSetData($statusButton, "Status Looking: " & $hasTarget[0])


		If $hasTarget[0] == 1 Then

			;Spells to use while you have a target
			If $onTarget == False Then
			   $onTarget = True
			   ClassPullImp($statusButton)
			EndIf
			GUICtrlSetData($statusButton, "Status:  Attacking")
			ClassAttack()
			ClassTimerImp($statusButton)
			Local $timeoutDif = Timerdiff($timeoutTimer)
			If $timeoutDif > ($timeout * 1000) Then
			   GUICtrlSetData($statusButton, "Status:  Timed out.")
			   Sleep(250)
			   ControlSend("Dark Age", "", "", "{ESC}")
			   Sleep(1000)
			   $timeoutTimer = TimerInit()
			EndIf
		Else
			;No Target stuff here.
			$timeoutTimer = TimerInit()
			ClassBuffImp($statusButton)
  		    ControlSend("Dark Age", "", "", GUICtrlRead($tabOrAssistComboBox))
			Sleep(100)
			ControlSend("Dark Age", "", "", "f") ; face
			Sleep(100)
			$onTarget = false
			Sleep(300)
			; Tab to search for target?
			GUICtrlSetData($statusButton, "Status:  Searching for Target.")
			sleep(1500)

		EndIf
	EndIf

WEnd


Func TogglePause()
    $paused = NOT $paused
    While $Paused
        sleep(100)
        GUICtrlSetData($statusButton, "Status:  Paused")
    WEnd
    GUICtrlSetData($statusButton, "Status: Ready")
EndFunc
