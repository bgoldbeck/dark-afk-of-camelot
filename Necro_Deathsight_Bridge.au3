
; # SECONDS between class timer in combat. (EG: Quickcast spell every 30 seconds)
Local $timerRecast = 30

; # MINUTES between recast class buffs.
Local $buffRecast = 19

; # SECONDS between your ability to pull a mob.
local $pullRecast = 3


; Spells on bar
; Slot 1: Power Tap
; Slot 2: Target Pet Macro (Shift click on pet name)
; Slot 3: Quickcast
; Slot 4: Shade Life Steal
; Slot 5: Pet Heal Over Time
; Slot 6: ABS Buff
; Slot 7: Dex Buff
; Slot 8: Str Buff
; Slot 9: D/Q Buff
; Slot 0: S/C Buff


; This is called every '$timerRecast' seconds while in combat.
Func ClassTimer()
   ; Start Editing here
   Sleep(700)
   ControlSend("Dark Age", "", "", "3") ; Quickcast
   Sleep(200)
   ControlSend("Dark Age", "", "", "1") ; Power Tap
   Sleep(1800)
   ControlSend("Dark Age", "", "", "5") ; Pet Heal Over Time
   Sleep(100)
   ControlSend("Dark Age", "", "", "1") ; Power Tap
   Sleep(1700)
   ControlSend("Dark Age", "", "", "4") ; Shade Life Steal
   Sleep(100)
   ControlSend("Dark Age", "", "", "1") ; Power Tap
   Sleep(900)
EndFunc


; This is called every time the script gains a new target.
Func ClassPull()
   ; Start Editing here
   ControlSend("Dark Age", "", "", "1") ; Power Tap
   Sleep(1200)
   ControlSend("Dark Age", "", "", "1") ; Power Tap
   Sleep(1700)
   ControlSend("Dark Age", "", "", "1") ; Power Tap
   Sleep(1600)
   ControlSend("Dark Age", "", "", "1") ; Power Tap
   Sleep(1600)
   ControlSend("Dark Age", "", "", "1") ; Power Tap
   Sleep(1600)
   ControlSend("Dark Age", "", "", "1") ; Power Tap
   Sleep(1600)
EndFunc


; This is called on repeat while the script has a target.
Func ClassAttack()
   ; Start Editing here
   ControlSend("Dark Age", "", "", "4") ; Shade Life Steal
   Sleep(750)
EndFunc


; This is called every '$buffRecast' minutes and only while the script has no target.
Func ClassBuff()
   ; Start Editing here
   ControlSend("Dark Age", "", "", "2") ; Target Self Macro
   Sleep(750)
   ControlSend("Dark Age", "", "", "6") ; Buff
   Sleep(1500)
   ControlSend("Dark Age", "", "", "7") ; Buff
   Sleep(1500)
   ControlSend("Dark Age", "", "", "8") ; Buff
   Sleep(2500)
   ControlSend("Dark Age", "", "", "9") ; Buff
   Sleep(1500)
   ControlSend("Dark Age", "", "", "0") ; Buff
   Sleep(3000)
EndFunc

; This is called every rotation, regardless of combat or not.
Func ClassUpdate()

EndFunc

; ----------------------------------------------------------------------------------------------;
; DO NOT EDIT ANYTHING BELOW THIS LINE
; ----------------------------------------------------------------------------------------------;
Local $buffTimer = 999999999
Local $classTimer = 999999999
Local $pullTimer = 99999

Func ClassPullImp($statusButton)
   Local $pullTimerDif = Timerdiff($pullTimer)
   While $pullTimerDif < ($pullRecast * 1000)
	  GUICtrlSetData($statusButton, "Status:  Waiting to pull.")
   WEnd
	  $pullTimer = Timerinit()
	  GUICtrlSetData($statusButton, "Status:  Pulling")
   ClassPull()
EndFunc

Func ClassBuffImp($statusButton)
   Local $buffTimerDif = Timerdiff($buffTimer)
   If $buffTimerDif > ($buffRecast * 60000) Then
	  GUICtrlSetData($statusButton, "Status:  Buffing.")
	  ClassBuff()
	  $buffTimer = Timerinit()
   EndIf
EndFunc

Func ClassTimerImp($statusButton)
   Local $classTimerDif = Timerdiff($classTimer)
   If $classTimerDif > ($timerRecast * 1000) Then
	  GUICtrlSetData($statusButton, "Status:  Hitting Class Timers")
	  ClassTimer()
	  $classTimer = Timerinit()
   EndIf
EndFunc